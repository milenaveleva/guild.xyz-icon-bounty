# **My submission for guild.xyz's 20 pieces of DAO-related pixel art bounty**

## figma presentation can be found [here](https://www.figma.com/proto/3ZYffqAC8iOGwNqKsFtWUu/guild.xyz-icon-bounty?scaling=contain&page-id=0%3A1&starting-point-node-id=10%3A2&node-id=10%3A2)

## previews:
![figma_slide1](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview1.png?raw=true)
![figma_slide2](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview2.png?raw=true)
![figma_slide3](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview3.png?raw=true)
![figma_slide4](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview4.png?raw=true)
![figma_slide5](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview5.png?raw=true)
![figma_slide6](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview6.png?raw=true)
![figma_slide7](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview7.png?raw=true)
![figma_slide8](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview8.png?raw=true)
![figma_slide9](https://gitlab.com/milenaveleva/guild.xyz-icon-bounty/-/raw/main/_previews/preview9.png?raw=true)

##raw files: can be found in this repo
